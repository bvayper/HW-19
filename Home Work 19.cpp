#include <iostream>
#include <string>

class Animal
{
private:
	std::string x;
public:
	virtual std::string Voice()
	{
		std::cout << '\n' << "Animal say ... something." << '\n';
		return x;
	}
	virtual ~Animal() 
	{
		Destruct();
	//	std::cout << "Destruct Animal" <<'\n';
	}
	virtual void Destruct() { std::cout << "Destruct Animal" << std::endl; }
};

class Dog : public Animal
{
private:
	std::string x;
public:
	void Destruct()override { std::cout << "Doggy Disappear ?!" << std::endl; }

	virtual std::string  Voice()override
	{
		std::cout << '\n' << "Woof!" << '\n';
		return x;
	}

	~Dog()override
	{
		Destruct();
	}
	/*~Dog()override
	{
		std::cout << "Destruct Doggy"<<'\n';
	}*/

};

class Cat : public Animal
{
private:
	std::string x;
public:
	void Destruct()override { std::cout << "Kitten Disappear ?!" << std::endl; }

	virtual std::string Voice()override
	{
		std::cout << '\n' << "Meow" << '\n';
		return x;
	}

	~Cat()override
	{
		Destruct();
	}
	/*{
		std::cout << "Destruct Cat" << '\n';
	}*/

};

class Hog : public Animal
{
private:
	std::string x;
public:
	void Destruct()override { std::cout << "Piggy Disappear ?!" << std::endl; }

	virtual std::string Voice()override
	{
		std::cout << '\n' << "Oink-oink!" << '\n';
		return x;
	}

	~Hog()override
	{
		Destruct();
	}
	/*{
		std::cout << "Destruct Hog" << '\n';
	}*/

};

class Bee : public Animal
{
private:
	std::string x;
public:
	void Destruct()override { std::cout << "Honey Bee Disappear ?!" << std::endl; }

	virtual std::string Voice()override
	{
		std::cout << '\n' << "Buzz-buzz" << '\n';
		return x;
	}

	~Bee()override
	{
		Destruct();
	}
	/*{
		std::cout << "Destruct Bee" << '\n';
	}*/

};

class Lion : public Animal
{
private:
	std::string x;
public:
	void Destruct()override { std::cout << "King of the Pride Disappear ?!" << std::endl; }

	virtual std::string Voice()override
	{
		std::cout << '\n' << "Roar!" << '\n';
		return x;
	}

	~Lion()override
	{
		Destruct();
	}
	/*{
		std::cout << "Destruct Lion" << '\n';
	}*/

};

int main()
{
	Animal** Creatures = new Animal * [5];
	Creatures[0] = new Bee();
	Creatures[1] = new Cat();
	Creatures[2] = new Dog();
	Creatures[3] = new Hog();
	Creatures[4] = new Lion();
	for (int i = 0; i < 5; i++)
	{
		Creatures[i]->Voice();
		delete Creatures[i];
	}
	delete[] Creatures;
	return 0;
}